$(function(){
   'use strict'

	// Message
	$("#but1").on("click", function(e){
		$('body').removeClass('timer-alert');
		var message = $("#message").val();
		if(message == ""){
			message  = "New Notification from Azea";
		}
		swal(message);
	});
	// With message and title
	$("#but2").on("click", function(e){
		$('body').removeClass('timer-alert');
		var message = $("#message").val();
		var title = $("#title").val();
		if(message == ""){
			message  = "New Notification from Azea";
		}
		if(title == ""){
			title = "Notifiaction Styles";
		}
		swal(title,message);
	});

	// Show image
	$("#but3").on("click", function(e){
		$('body').removeClass('timer-alert');
		var message = $("#message").val();
		var title = $("#title").val();
		if(message == ""){
			message  = "New Notification from Azea";
		}
		if(title == ""){
			title = "Notifiaction Styles";
		}
		swal({
			title: title,
			text: message,
			imageUrl: '../assets/images/brand/favicon.png'
		});
	});

	// Timer
	$("#but4").on("click", function(e){
		$('body').addClass('timer-alert');
		var message = $("#message").val();
		var title = $("#title").val();
		if(message == ""){
			message  = "New Notification from Azea";
		}
		if(title == ""){
			title = "Notifiaction Styles";
		}
		message += "(close after 2 seconds)";
		swal({
			title: title,
			text: message,
			timer: 2000,
			showConfirmButton: false
		});
	});

	//
	$("#click").on("click", function(e){
		$('body').removeClass('timer-alert');
		var type = $("#type").val();
		swal({
			title: "Notifiaction Styles",
			text: "New Notification from Azea",
			type: type
		});
	});

	// Prompt
	$("#prompt").on("click", function(e){
		$('body').removeClass('timer-alert');
		swal({
			title: "Notification Alert",
			text: "your getting some notification from mail please check it",
			type: "input",
			showCancelButton: true,
			closeOnConfirm: true,
			inputPlaceholder: "Your message"
		},function(inputValue){


			if (inputValue != "") {
				swal("Input","You have entered : " + inputValue);

			}
		});
	});

	// Confirm
	$("#confirm").on("click", function(e){
		$('body').removeClass('timer-alert');
		swal({
			title: "Notifiaction Styles",
			text: "New Notification from Azea",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: 'Exit',
			cancelButtonText: 'Stay on the page'
		});
	});


	$("#click").on("click", function(e){
		swal('Congratulations!', 'Your message has been succesfully sent', 'success');
	});
	$("#ls-confirm").on("click", function(e){
		swal('', '로그아웃 되었습니다.', 'success');
		$('.sa-button-container .confirm').text('확인')
	});
	$("#ls-confirm2").on("click", function(e){
		swal('', '정상 등록 되었습니다.', 'success');
		$('.sa-button-container .confirm').text('확인')
	});
	$("#ls-confirm3").on("click", function(e){
		swal('', '정상 승인 되었습니다.', 'success');
		$('.sa-button-container .confirm').text('확인')
	});
	$("#ls-confirm4").on("click", function(e){
		swal('', '삭제되었습니다.', 'success');
		$('.sa-button-container .confirm').text('확인')
	});
	$("#simple1").on("click", function(e){
		swal('해당 메뉴에 권한이 없습니다.');
		$('.sa-button-container .confirm').text('확인')
	});
	$("#simple2").on("click", function(e){
		swal('일시적 오류가 발생하였습니다.');
		$('.sa-button-container .confirm').text('확인')
	});
	$("#use1").on("click", function(e){
		swal({
			title: "사용자 정보가 다릅니다.\n 확인 후 다시 입력하세요.",
			type: "warning",
			showCancelButton: false,
			confirmButtonText: '확인'
		});
	});
	$("#use2").on("click", function(e){
		swal({
			title: "아이디 또는 비밀번호 오류! \n 확인 후 다시 입력하세요.",
			type: "warning",
			showCancelButton: false,
			confirmButtonText: '확인'
		});
	});
	$("#manage").on("click", function(e){
		swal({
			title: "등록하신 정보로 관리자 신청을 하시겠습니까?\n 요청 후 1~2일정도 소유됩니다.",
			showCancelButton: true,
			confirmButtonText: '확인',
			cancelButtonText: '취소'
		});
	});
	$("#click1").on("click", function(e){
		swal({
			title: "Some Risk File Is Founded",
			text: "Some Virus file is detected your system going to be in Risk",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: 'Exit',
			cancelButtonText: 'Stay on the page'
		});
	});
	$("#click2").on("click", function(e){
		swal({
			title: "Something Went Wrong",
			text: "Please fix the issue the issue file not loaded & items not found",
			type: "error",
			showCancelButton: true,
			confirmButtonText: 'Exit',
			cancelButtonText: 'Stay on the page'
		});
	});
	$("#dele1").on("click", function(e){
		swal({
			title: "선택 하신 게시글을 삭제하시겠습니까?",
			text: "삭제 후 복구되지 않습니다.",
			type: "error",
			showCancelButton: true,
			confirmButtonText: '확인',
			cancelButtonText: '취소'
		});
	});
	$("#dele2").on("click", function(e){
		swal({
			title: "작성을 취소 하시겠습니까?",
			text: "지금까지 작업한 내용은 저장되지 않습니다.",
			type: "error",
			showCancelButton: true,
			confirmButtonText: '확인',
			cancelButtonText: '취소'
		});
	});
	$("#dele3").on("click", function(e){
		swal({
			title: "관리자 승인을 거절 되었습니다.",
			type: "error",
			showCancelButton: false,
			confirmButtonText: '확인'
		});
	});
	$("#dele4").on("click", function(e){
		swal({
			title: "관리자 승인을 거절하시겠습니까?",
			type: "error",
			showCancelButton: true,
			confirmButtonText: '확인',
			cancelButtonText: '취소'
		});
	});
	$("#use3").on("click", function(e){
		swal({
			title: "선택된 항목이 없습니다.",
			type: "info",
			showCancelButton: false,
			confirmButtonText: '확인'
		});
	});
	$("#use4").on("click", function(e){
		swal({
			title: "관리자 신규 요청 건 \n 관리자 관리 > 계정관리” 에서 \n 확인 하실 수 있습니다.",
			type: "info",
			showCancelButton: false,
			confirmButtonText: '확인'
		});
	});
	$("#click3").on("click", function(e){
		swal({
			title: "Notification Alert",
			text: "your getting some notification from mail please check it",
			type: "info",
			showCancelButton: true,
			confirmButtonText: 'Exit',
			cancelButtonText: 'Stay on the page'
		});
	});
});